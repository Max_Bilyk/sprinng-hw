import {Route, Switch} from "react-router-dom";
import Home from "../pages/Home/Home";
import DeleteCustomer from "../pages/DeleteCustomer/DeleteCustomer";
import CreateCustomer from "../pages/CreateCustomer/CreateCustomer";
import UpdateCustomer from "../pages/UpdateCustomer/UpdateCustomer";
import CreateAccount from "../pages/CreateAccount/CreateAccount";
import DeleteAccount from "../pages/DeleteAccount/DeleteAccount";
import UpWithdrawMoney from "../pages/UpOrWithdrawMoney/UpWithdrawMoney";
import TransferMoney from "../pages/TransferMoney/TransferMoney";

export default function Router() {
    return(
        <Switch>
            <Route exact path={"/"}>
                <Home/>
            </Route>
            <Route exact path={"/deleteCustomer"}>
                <DeleteCustomer/>
            </Route>
            <Route exact path={"/createCustomer"}>
                <CreateCustomer/>
            </Route>
            <Route exact path={"/updateCustomer"}>
                <UpdateCustomer/>
            </Route>
            <Route exact path={"/createAccount"}>
                <CreateAccount/>
            </Route>
            <Route exact path={"/deleteAccount"}>
                <DeleteAccount/>
            </Route>
            <Route exact path={"/upMoney"}>
                <UpWithdrawMoney operation={"up"}/>
            </Route>
            <Route exact path={"/withdrawMoney"}>
                <UpWithdrawMoney operation={"withdraw"}/>
            </Route>
            <Route exact path={"/transferMoney"}>
                <TransferMoney/>
            </Route>
        </Switch>
    )
}