import './App.css';
import Router from "./router/Router";
import Navbar from "./components/Header/Navbar";
import {useDispatch} from "react-redux";
import {getCustomers} from "./redux/actions/customerAction";
import {Layout} from "antd";
import {getAccounts} from "./redux/actions/accountAction";

function App() {
    const dispatch = useDispatch();
    dispatch(getAccounts());
    dispatch(getCustomers());

    return (
        <>
            <Layout>
                <Navbar/>
                <Router/>
            </Layout>
        </>
    );
}

export default App;
