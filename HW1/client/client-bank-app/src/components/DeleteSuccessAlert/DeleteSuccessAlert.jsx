import {Alert, Button} from "antd";

const DeleteSuccessAlert = ({isDeleted, id, entity}) => {
    return(
        <div style={{marginBottom: "500px"}}>
            {
                isDeleted === true && id !== 0 ?
                    <Alert message={`${entity} with id ${id} has been deleted`} type="success" showIcon
                           action={<Button size="small" primary><a href="/">Return to main page</a></Button>}/> :
                    <div style={{display: "none"}}></div>
            }
        </div>
    )
}

export default DeleteSuccessAlert;