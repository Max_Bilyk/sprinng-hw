import {Menu, Row} from "antd";
import {
    DeleteOutlined, DownloadOutlined, FolderAddOutlined,
    HomeOutlined,
    SolutionOutlined,
    TeamOutlined, TransactionOutlined, UploadOutlined,
    UserAddOutlined, UserDeleteOutlined,
    UserSwitchOutlined
} from "@ant-design/icons";

const Navbar = () => {

    const {SubMenu} = Menu;

    return(
        <>
            <div>
                <Row justify="end">
                    <Menu className="header-navbar" mode="horizontal" selectable={false}
                          style={{
                              background: "#fff",
                              width: "100%",
                              lineHeight: "70px",
                              display: "flex",
                              justifyContent: "center",
                              fontSize: "19px"
                          }}>
                        <Menu.Item key="main" icon={<HomeOutlined />}>
                            <a href={'/'}>Home</a>
                        </Menu.Item>
                        <SubMenu key="customers" icon={<TeamOutlined/>} title={"Customers"}>
                            <Menu.ItemGroup title={"Customers"}>
                                <Menu.Item key="deleteCustomers" icon={<UserDeleteOutlined />}>
                                    <a href={'/deleteCustomer'}>Delete Customer</a>
                                </Menu.Item>
                                <Menu.Item key="createCustomer" icon={<UserAddOutlined />}>
                                    <a href={"/createCustomer"}>Create Customer</a>
                                </Menu.Item>
                                <Menu.Item key="updateCustomer" icon={<UserSwitchOutlined />}>
                                    <a href={"/updateCustomer"}>Update Customer</a>
                                </Menu.Item>
                            </Menu.ItemGroup>
                        </SubMenu>
                        <SubMenu key="accounts" icon={<SolutionOutlined/>} title={"Accounts"}>
                            <Menu.ItemGroup title={"Accounts"}>
                                <Menu.Item key="createAccount" icon={<FolderAddOutlined />}>
                                    <a href={'/createAccount'}>Create Account</a>
                                </Menu.Item>
                                <Menu.Item key="deleteAccount" icon={<DeleteOutlined />}>
                                    <a href={'/deleteAccount'}>Delete Account</a>
                                </Menu.Item>
                                <Menu.Item key="upMoney" icon={<UploadOutlined />}>
                                    <a href={'/upMoney'}>Up Account</a>
                                </Menu.Item>
                                <Menu.Item key="withdrawMoney" icon={<DownloadOutlined />}>
                                    <a href={"/withdrawMoney"}>Withdraw Account</a>
                                </Menu.Item>
                                <Menu.Item key="transferMoney" icon={<TransactionOutlined />}>
                                    <a href={"/transferMoney"}>Transfer Money</a>
                                </Menu.Item>
                            </Menu.ItemGroup>
                        </SubMenu>
                    </Menu>
                </Row>
            </div>

        </>
    )
}

export default Navbar;