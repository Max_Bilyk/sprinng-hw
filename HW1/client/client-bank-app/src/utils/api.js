import axios from 'axios';

const api = {
    get: async (url, headers) => {
        return await axios.get(url, headers);
    },
    post: async (url, data, headers) => {
        return await axios.post(url, data, { headers });
    },
    put: async (url, data, headers) => {
        return await axios.put(url, data, { headers });
    },
    delete: async (url, data, headers) => {
        return await axios.delete(url, { data, headers });
    },
};

export default api;