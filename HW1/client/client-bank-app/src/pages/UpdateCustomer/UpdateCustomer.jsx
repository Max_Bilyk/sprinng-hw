import "../../App.css";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form, Select} from "antd";
import {useState} from "react";
import {rules} from "../../utils/rules";
import {Option} from "antd/es/mentions";
import {getCustomers, updateCustomer} from "../../redux/actions/customerAction";
import Input from "antd/es/input/Input";
import api from "../../utils/api";

const UpdateCustomer = () => {
    const customers = useSelector(state => state.customer.customers);
    const dispatch = useDispatch();

    const [customerId, setCustomerId] = useState(0);
    const [customer, setCustomer] = useState(null);
    const [name, setName] = useState(null);
    const [email, setEmail] = useState(null);
    const [age, setAge] = useState(null);

    const handleCustomers = (value) => setCustomerId(value)
    const fetchCustomer = () => {
        api.get(`http://localhost:9000/customers/${customerId}`)
            .then(response => setCustomer(response.data))
            .catch(error => console.error("Set customer at UpdateCustomer.jsx ===> ", error))
    }

    const fetchingUpdateCustomer = () => {
        dispatch(updateCustomer(
            customer?.id,
            name || customer?.name,
            email || customer?.email,
            age || customer?.age,
            customer.accounts));
        dispatch(getCustomers())
    }
    
    return(
        <div className={"container"}>
            <div className="title-wrapper">
                <h1>Update Customer</h1>
            </div>

            <Form onFinish={fetchCustomer} className={"form-data"}>
                <Form.Item
                    label="Choose customer"
                    name="customer"
                    rules={[rules.required('Please select customer!')]}
                >
                    <Select defaultValue="Select customer to update"
                            style={{width: "100%"}} onChange={(e) => handleCustomers(e)}>
                        {
                            customers?.map((e, index) => {
                                return (<Option key={`${index}${e?.name}`} value={e?.id}>Id: {e?.id}, Name: {e?.name}</Option>)
                            })
                        }
                    </Select>
                </Form.Item>

                <Button htmlType={"submit"} type={"primary"}>Submit</Button>
            </Form>

            {
                customer !== null ?
                    <Form onFinish={fetchingUpdateCustomer} className={"form-data"}>
                        <Form.Item label="Name" name={"name"}>
                            <Input value={name}
                                   minLength="3"
                                   maxLength="32"
                                   onChange={(e) => setName(e.target.value)}
                                   placeholder="Enter customer name"/>
                        </Form.Item>

                        <Form.Item label="Email" name={"email"}>
                            <Input value={email}
                                   minLength="3"
                                   maxLength="32"
                                   onChange={(e) => setEmail(e.target.value)}
                                   type={"email"}
                                   placeholder="Enter customer email"/>
                        </Form.Item>

                        <Form.Item label="Age" name={"age"}>
                            <Input value={age}
                                   type={"number"}
                                   style={{width: "100%"}}
                                   onChange={(e) => setAge(e.target.value)}
                                   placeholder="Enter customer age"/>
                        </Form.Item>

                        <Button type={"primary"} htmlType={"submit"}>Update</Button>
                    </Form> : <div style={{display: "none"}}></div>
            }
        </div>
    )
}

export default UpdateCustomer;