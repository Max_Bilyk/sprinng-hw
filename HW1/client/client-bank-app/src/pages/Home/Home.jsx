import {useSelector} from "react-redux";
import "./home.css"
import {Table} from "antd";
import "../../utils/dataColumns/customerDataColumn";
import {customerDataColumn} from "../../utils/dataColumns/customerDataColumn";
import {accountDataColumn} from "../../utils/dataColumns/accountDataColumn";

const Home = () => {
    const customers = useSelector(state => state.customer.customers);
    const accounts = useSelector(state => state.account.accounts);

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }

    return (
        <div className="container">
                <div className={"page-title"}>
                    <h1>Customers</h1>
                </div>

                <div>
                    <Table columns={customerDataColumn} dataSource={customers} onChange={onChange}/>
                </div>

                <div className={"page-title"}>
                    <h1>Customers` accounts</h1>
                </div>

                <div>
                    <Table columns={accountDataColumn} dataSource={accounts} onChange={onChange}/>
                </div>
        </div>
    )
};

export default Home;