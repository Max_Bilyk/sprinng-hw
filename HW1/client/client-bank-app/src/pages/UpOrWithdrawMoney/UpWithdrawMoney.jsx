import "../../App.css"
import {Button, Form, Select} from "antd";
import {useSelector} from "react-redux";
import {rules} from "../../utils/rules";
import {Option} from "antd/es/mentions";
import {useState} from "react";
import Input from "antd/es/input/Input";
import axios from "axios";

const UpWithdrawMoney = ({operation}) => {
    const customers = useSelector(state => state.customer.customers);
    const accounts = useSelector(state => state.account.accounts);

    const [customerId, setCustomerId] = useState(0);
    const [accountId, setAccountId] = useState(0);
    const [amount, setAmount] = useState(0.00);

    const handleChangeCustomer = (value) => setCustomerId(value);
    const handleChangeAccount = (value) => setAccountId(value);

    const filteredAccounts = accounts.filter(a => {
        return a.customer === customerId;
    });

    console.log(filteredAccounts)

    const filteredAccount = accounts.filter(account => account.id === accountId);

    const fetchUpAccount = () => {
        axios.patch(`http://localhost:9000/account/up?number=${filteredAccount[0]?.number}&amount=${amount}`)
            .then(res => res.status >= 200 && res.status <= 304 ? alert("Success") : alert("Something went wrong..."))
            .catch(error => console.error("Up Account: ", error));
    }

    const fetchWithdrawAccount = () => {
        axios.patch(`http://localhost:9000/account/withdraw?number=${filteredAccount[0]?.number}&amount=${amount}`)
            .then(res => res.status >= 200 && res.status <= 304 ? alert("Success") : alert("Something went wrong..."))
            .catch(error => console.error("Withdraw Account: ", error));
    }

    return(
        <div className="container">
            <div className="title-wrapper">
                {operation === "withdraw" ?
                    <h1>Withdraw Account</h1> :
                    <h1>Up Account</h1>
                }
            </div>

            <Form>
                <Form.Item label="Choose account`s customer" name="customer"
                           rules={[rules.required('Please choose customer!')]}>
                    <Select defaultValue="Select customer" onChange={handleChangeCustomer} style={{width: "100%"}}>
                        {customers?.map(customer => {
                            return (
                                <Option key={customer?.name} value={customer.id}>
                                    Id: {customer?.id}, Name: {customer?.name}
                                </Option>
                            )
                        })}
                    </Select>
                </Form.Item>
            </Form>

            {filteredAccounts.length > 0 ?
                <Form onFinish={operation === "up" ? fetchUpAccount : fetchWithdrawAccount}>
                    <Form.Item label="Choose account" name="account" rules={[rules.required('Please choose account!')]}>
                        <Select defaultValue="Select account" onChange={handleChangeAccount} style={{width: "100%"}}>
                            {filteredAccounts?.map(account => {
                                return (
                                    <Option key={account?.number} value={account?.id}>
                                        Id: {account?.id}, Acc number: {account?.number}, Balance: {account.balance} {account.currency}
                                    </Option>
                                )
                            })}
                        </Select>
                    </Form.Item>

                    <Form.Item name={"amount"} label={"Input how much money you want to withdraw/up on an account"} rules={[rules.required("Please enter amount of money")]}>
                        <Input
                            value={amount}
                            onChange={(e) => setAmount(e.target.value)}
                            style={{width: "100%"}}
                            placeholder="Enter amount of money"
                            type={"number"}
                        />
                    </Form.Item>
                    <Button style={{width: "100%"}} htmlType={"submit"} type={"primary"}>Up Account</Button>
                </Form> : <div style={{display: "none"}}/>
            }
        </div>
    )
}

export default UpWithdrawMoney;