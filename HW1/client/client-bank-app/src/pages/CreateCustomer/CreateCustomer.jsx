import "../../App.css";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {createCustomer} from "../../redux/actions/customerAction";
import {Alert, Button, Form} from "antd";
import Input from "antd/es/input/Input";
import {rules} from "../../utils/rules";

const CreateCustomer = () => {
    const dispatch = useDispatch();
    const createdCustomer = useSelector(state => state.customer.createdCustomer);

    const [name, setName] = useState("");
    const [age, setAge] = useState(0);
    const [email, setEmail] = useState("");
    const accounts = [];

    const fetchCustomer = () => {
        dispatch(createCustomer(name, email, age, accounts))
    }

    return(
        <div className="container" style={{marginBottom: "400px"}}>
            <div className={"title-wrapper"}>
                <h1>Create customer</h1>
            </div>

            <Form onFinish={fetchCustomer} className="form-data">
                <Form.Item label="Name" name={"name"} rules={[rules.required('Please input customer name')]}>
                    <Input value={name}
                           minLength="3"
                           maxLength="32"
                           onChange={(e) => setName(e.target.value)}
                           placeholder="Enter customer name"/>
                </Form.Item>

                <Form.Item label="Email" name={"email"} rules={[rules.required('Please input customer email')]}>
                    <Input value={email}
                           minLength="3"
                           maxLength="32"
                           onChange={(e) => setEmail(e.target.value)}
                           type={"email"}
                           placeholder="Enter customer email"/>
                </Form.Item>

                <Form.Item label="Age" name={"age"} rules={[rules.required('Please input customer age')]}>
                    <Input value={age}
                           type={"number"}
                           style={{width: "100%"}}
                           onChange={(e) => setAge(e.target.value)}
                           placeholder="Enter customer age"/>
                </Form.Item>

                <Button type={"primary"} htmlType={"submit"}>Create Customer</Button>
            </Form>

            <div>
                {
                    createdCustomer ?
                        <Alert message={`Customer ${createdCustomer.name} with id ${createdCustomer.id} has been created`} type="success" showIcon
                               action={<Button size="small" primary><a href="/">Return to main page</a></Button>}/> :
                        <div style={{display: "none"}}></div>
                }
            </div>
        </div>
    )
}

export default CreateCustomer;