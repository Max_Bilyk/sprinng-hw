import "../../App.css";
import {Button, Form, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {rules} from "../../utils/rules";
import {Option} from "antd/es/mentions";
import {useState} from "react";
import {deleteAccountById} from "../../redux/actions/accountAction";
import DeleteSuccessAlert from "../../components/DeleteSuccessAlert/DeleteSuccessAlert";

const DeleteAccount = () => {
    const accounts = useSelector(state => state.account.accounts);
    const isAccountDeleted = useSelector(state => state.account.deletedAccount);

    const dispatch = useDispatch();

    const [accountId, setAccountId] = useState(0);

    
    const handleChange = (id) => setAccountId(id);
    const fetchDeleteAccount = () => dispatch(deleteAccountById(accountId));

    return(
        <div className="container">
            <div className="title-wrapper">
                <h1>Delete Account</h1>
            </div>

            <Form className="form-data" onFinish={fetchDeleteAccount}>
                <Form.Item label="Account" name={"id"} rules={[rules.required('Please choose account')]}>
                    <Select defaultValue="Select account to delete" onChange={handleChange} style={{width: "100%"}}>
                        {accounts?.map(account => {
                            return (
                                <Option key={account?.number} value={account?.id}>
                                    Id: {account?.id}, Number: {account?.number}
                                </Option>
                            )
                        })}
                    </Select>
                </Form.Item>

                <Button type={"primary"} htmlType={"submit"} danger style={{width: "100%"}}>Delete Account</Button>
            </Form>
            <DeleteSuccessAlert entity={"Account"} id={accountId} isDeleted={isAccountDeleted}/>
        </div>
    )
}

export default DeleteAccount;