import "../../App.css";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form, Select} from "antd";
import {rules} from "../../utils/rules";
import {Option} from "antd/es/mentions";
import {useState} from "react";
import Input from "antd/es/input/Input";
import {createAccount} from "../../redux/actions/accountAction";

const CreateAccount = () => {
    const customers = useSelector(state => state.customer.customers);
    const dispatch = useDispatch();

    const [customerId, setCustomerId] = useState(null);
    const [currency, setCurrency] = useState("");
    const [balance, setBalance] = useState(0.00);

    const handleAccOwner = (id) => setCustomerId(id);
    const handleCurrency = (currency) => setCurrency(currency);
    const fetchAccount = () => {dispatch(createAccount(customerId, currency, balance));}

    return(
        <div className="container">
            <div className="title-wrapper">
                <h1>Create Account</h1>
            </div>

            <Form className="form-data" onFinish={fetchAccount}>
                <Form.Item label="Customer" name={"customerId"} rules={[rules.required('Please choose customer')]}>
                    <Select defaultValue="Select customer for account creating" onChange={handleAccOwner} style={{width: "100%"}}>
                        {customers?.map(customer => {
                            return (
                                <Option key={customer?.name} value={customer?.id}>
                                    Id: {customer?.id}, Name: {customer?.name}
                                </Option>
                            )
                        })}
                    </Select>
                </Form.Item>

                <Form.Item label="Currency" name={"currency"} rules={[rules.required('Please choose account currency')]}>
                    <Select defaultValue="Select currency" onChange={handleCurrency} style={{width: "100%"}}>
                        <Option key={"USD"} value={"USD"}>
                            USD
                        </Option>
                        <Option key={"EUR"} value={"EUR"}>
                            EUR
                        </Option>
                        <Option key={"UAH"} value={"UAH"}>
                            UAH
                        </Option>
                        <Option key={"CHF"} value={"CHF"}>
                            CHF
                        </Option>
                        <Option key={"GBP"} value={"GBP"}>
                            GBP
                        </Option>
                    </Select>
                </Form.Item>
                <Form.Item label="Balance" name={"balance"} rules={[rules.required('Please enter balance')]}>
                    <Input value={balance}
                           type={"number"}
                           style={{width: "100%"}}
                           onChange={(e) => setBalance(e.target.value)}
                           placeholder="Balance"/>
                </Form.Item>

                <Button style={{marginBottom: "500px"}} htmlType={"submit"} type={"primary"}>Create Account</Button>
            </Form>
        </div>
    )
}

export default CreateAccount;