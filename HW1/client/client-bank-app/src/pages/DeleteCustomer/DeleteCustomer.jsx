import "../../App.css";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Form, Select} from "antd";
import {Option} from "antd/es/mentions";
import {deleteCustomerById} from "../../redux/actions/customerAction";
import {rules} from "../../utils/rules";
import DeleteSuccessAlert from "../../components/DeleteSuccessAlert/DeleteSuccessAlert";


const DeleteCustomer = () => {
    const customers = useSelector(state => state.customer.customers);
    const isCustomerDeleted = useSelector(state => state.customer.deletedCustomer);


    const dispatch = useDispatch();

    const [customerId, setCustomerId] = useState(0);

    const handleChange = (id) => setCustomerId(id);
    const handleCustomerDelete = () => dispatch(deleteCustomerById(customerId));

    return (
        <div className="container">
            <div className={"title-wrapper"}>
                <h1>Delete Customers</h1>
            </div>

            <Form onFinish={handleCustomerDelete} className={"form-data"}>
                <Form.Item label="Choose customer" name="customer"
                           rules={[rules.required('Please input your defeats!')]}>
                    <Select defaultValue="Select customer to delete" onChange={handleChange} style={{width: "100%"}}>
                        {customers?.map(customer => {
                            return (
                                <Option key={customer?.name} value={customer?.id}>
                                    Id: {customer?.id}, Name: {customer?.name}
                                </Option>
                            )
                        })}
                    </Select>
                </Form.Item>

                <Button htmlType="submit" style={{width: "100%", marginBottom: "20px"}} type={"primary"} danger>Delete Customer</Button>
            </Form>

            <DeleteSuccessAlert entity={"Customer"} id={customerId} isDeleted={isCustomerDeleted}/>
        </div>
    )
}

export default DeleteCustomer;