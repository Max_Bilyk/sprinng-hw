import "../../App.css";
import {Button, Form, Select} from "antd";
import {useSelector} from "react-redux";
import {useState} from "react";
import {rules} from "../../utils/rules";
import {Option} from "antd/es/mentions";
import Input from "antd/es/input/Input";
import api from "../../utils/api";

const TransferMoney = () => {
    const accounts = useSelector(state => state.account.accounts);

    const [fromAccountNumber, setFromAccountNumber] = useState("");
    const [toAccountNumber, setToAccountNumber] = useState("");
    const [amount, setAmount] = useState(0.00);

    const excludeFromAccounts = accounts.filter(account => {
        return account.number !== fromAccountNumber;
    });

    const handleFromAccountNumber = (value) => setFromAccountNumber(value);
    const handleToAccountNumber = (value) => setToAccountNumber(value);

    const fetchTransfer = () => {
        api.put(`http://localhost:9000/account/transfer?fromNumber=${fromAccountNumber}&toNumber=${toAccountNumber}&amount=${amount}`)
            .then(response => response.status >= 200 && response.status <= 304 ? alert("Success") : alert("Something went wrong..."))
            .catch(error => console.error("Transfer Money: ", error))
    }

    return(
        <div className="container">
            <div className="title-wrapper">
                <h1>Transfer money</h1>
            </div>

            <Form onFinish={fetchTransfer}>
                <Form.Item label="Choose FROM account" name="fromAccount" rules={[rules.required('Please choose FROM account!')]}>
                    <Select defaultValue="Select account" onChange={handleFromAccountNumber} style={{width: "100%"}}>
                        {accounts?.map(account => {
                            return (
                                <Option key={account?.number} value={account?.number}>
                                    Id: {account?.id}, Acc number: {account?.number}, Balance: {account?.balance} {account?.currency}
                                </Option>
                            )
                        })}
                    </Select>
                </Form.Item>
                <Form.Item label="Choose TO account" name="toAccount" rules={[rules.required('Please choose TO account!')]}>
                    <Select defaultValue="Select account" onChange={handleToAccountNumber} style={{width: "100%"}}>
                        {excludeFromAccounts?.map(account => {
                            return (
                                <Option key={account?.number} value={account?.number}>
                                    Id: {account?.id}, Acc number: {account?.number}, Balance: {account?.balance} {account?.currency}
                                </Option>
                            )
                        })}
                    </Select>
                </Form.Item>

                <Form.Item name={"amount"} label={"Input how much money you want to to transfer"} rules={[rules.required("Please enter amount of money")]}>
                    <Input
                        value={amount}
                        onChange={(e) => setAmount(e.target.value)}
                        style={{width: "100%"}}
                        placeholder="Enter amount of money"
                        type={"number"}
                    />
                </Form.Item>

                <Button style={{width: "100%"}} htmlType={"submit"} type={"primary"}>Transfer</Button>
            </Form>
        </div>
    )
}

export default TransferMoney;