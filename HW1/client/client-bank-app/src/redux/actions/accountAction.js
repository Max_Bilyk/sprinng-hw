import api from "../../utils/api";
export const GET_ACCOUNTS = "GET_ACCOUNTS";
export const CREATE_ACCOUNT = "CREATE_ACCOUNT";
export const DELETE_ACCOUNT = "DELETE_ACCOUNT";

export const getAccounts = () => async (dispatch) => {
    await api
        .get("http://localhost:9000/account")
        .then(response => dispatch({ type: GET_ACCOUNTS, payload: response.data}))
        .catch(error => console.error(GET_ACCOUNTS, " error: ", error));
}
export const createAccount = (customerId, currency, balance) => async (dispatch) => {
    await api
        .post(`http://localhost:9000/customers/${customerId}/account`, {currency, balance})
        .then(response => dispatch({ type: CREATE_ACCOUNT, payload: response.data}))
        .catch(error => console.error(CREATE_ACCOUNT, " error: ", error));
}
export const deleteAccountById = (id) => async (dispatch) => {
    await api
        .delete(`http://localhost:9000/account/${id}`)
        .then(response => dispatch({ type: DELETE_ACCOUNT, payload: response.data}))
        .catch(error => console.error(DELETE_ACCOUNT, " error: ", error));
}