import api from "../../utils/api";
export const GET_CUSTOMERS = "GET_CUSTOMERS";
export const DELETE_CUSTOMER = "DELETE_CUSTOMER";
export const CREATE_CUSTOMER = "CREATE_CUSTOMER";
export const UPDATE_CUSTOMER = "UPDATE_CUSTOMER";

export const getCustomers = () => async (dispatch) => {
    await api
        .get(`http://localhost:9000/customers`)
        .then(response => dispatch({ type: GET_CUSTOMERS, payload: response.data}))
        .catch(error => console.error(GET_CUSTOMERS, " error: ", error));
};

export const deleteCustomerById = (id) => async (dispatch) => {
    await api
        .delete(`http://localhost:9000/customers/${id}`)
        .then(response => dispatch({ type: DELETE_CUSTOMER, payload: response.data}))
        .catch(error => console.error(DELETE_CUSTOMER, " error: ", error));
};
export const createCustomer = (name, email, age, accounts) => async (dispatch) => {
    await api
        .post(`http://localhost:9000/customers`, {name, email, age, accounts})
        .then(response => dispatch({ type: CREATE_CUSTOMER, payload: response.data}))
        .catch(error => console.error(CREATE_CUSTOMER, " error: ", error));
};
export const updateCustomer = (id, name, email, age, accounts) => async (dispatch) => {
    await api
        .put(`http://localhost:9000/customers`, {id, name, email, age, accounts})
        .then(response => dispatch({ type: UPDATE_CUSTOMER, payload: response.data}))
        .catch(error => console.error(UPDATE_CUSTOMER, " error: ", error));
}