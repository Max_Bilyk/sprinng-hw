import {
    CREATE_CUSTOMER,
    DELETE_CUSTOMER,
    GET_CUSTOMERS,
    UPDATE_CUSTOMER
} from "../actions/customerAction";

const CustomerInit = {
    customers: [],
    deletedCustomer: null,
    createdCustomer: {},
    updatedCustomer: {},
};

export const customerReducer = (customer = CustomerInit, action) => {
    switch (action.type) {
        case GET_CUSTOMERS :
            return {...customer, customers: action.payload}
        case DELETE_CUSTOMER :
            return {...customer, deletedCustomer: action.payload}
        case CREATE_CUSTOMER :
            return {...customer, createdCustomer: action.payload}
        case UPDATE_CUSTOMER :
            return {...customer, updatedCustomer: action.payload}
        default:
            return customer;
    }
};