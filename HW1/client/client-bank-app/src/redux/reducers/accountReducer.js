import {CREATE_ACCOUNT, DELETE_ACCOUNT, GET_ACCOUNTS} from "../actions/accountAction";

const AccountInit = {
    accounts: [],
    createdAccount: {},
    deletedAccount: null,
};

export const accountReducer = (account = AccountInit, action) => {
    switch (action.type) {
        case GET_ACCOUNTS :
            return {...account, accounts: action.payload}
        case CREATE_ACCOUNT :
            return {...account, createdAccount: action.payload}
        case DELETE_ACCOUNT :
            return {...account, deletedAccount: action.payload}
        default:
            return {...account}
    }
};