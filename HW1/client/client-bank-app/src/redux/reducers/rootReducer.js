import {combineReducers} from "redux";
import {customerReducer} from "./customerReducer";
import {accountReducer} from "./accountReducer";

export const rootReducer = combineReducers({
    customer: customerReducer,
    account: accountReducer,
});