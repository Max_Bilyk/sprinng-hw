package com.maxbilyk.hmw1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hmw1Application {

    public static void main(String[] args) {
        SpringApplication.run(Hmw1Application.class, args);
    }

}
