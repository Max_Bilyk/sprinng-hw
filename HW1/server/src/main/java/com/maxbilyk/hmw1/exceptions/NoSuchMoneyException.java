package com.maxbilyk.hmw1.exceptions;

public class NoSuchMoneyException extends RuntimeException{
    public NoSuchMoneyException(String msg) {
        super(msg);
    }
}
