package com.maxbilyk.hmw1.service;

import com.maxbilyk.hmw1.dao.AccountDao;
import com.maxbilyk.hmw1.domain.Account;
import com.maxbilyk.hmw1.exceptions.NoSuchMoneyException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {
    private final AccountDao accountDao;

    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public Account toUpAccount (String number, Double amount) {
        if (amount <= 0) throw new IllegalArgumentException("replenishment amount must be more then 0");
        Account account = accountDao.findByNumber(number);
        account.setBalance(account.getBalance() + amount);
        return accountDao.update(account);
    }

    public Account withdrawMoney (String number, Double amount) {
        Account account = accountDao.findByNumber(number);

        if (amount <= 0) throw new IllegalArgumentException("replenishment amount must be more then 0");
        if (account.getBalance() < amount) throw new NoSuchMoneyException("It`s not such money on your balance");

        account.setBalance(account.getBalance() - amount);
        return accountDao.update(account);
    }

    public Account transferMoney (String fromNumber, String toNumber, Double amount) {
        Account fromAccount = accountDao.findByNumber(fromNumber);
        Account toAccount = accountDao.findByNumber(toNumber);

        if (amount <= 0) throw new IllegalArgumentException("replenishment amount must be more then 0");
        if (fromAccount.getBalance() < amount) throw new NoSuchMoneyException("It`s not such money on your balance");

        fromAccount.setBalance(fromAccount.getBalance() - amount);
        toAccount.setBalance(toAccount.getBalance() + amount);

        accountDao.update(toAccount);
        return accountDao.update(fromAccount);
    }

    public List<Account> findAll () {
        return accountDao.findAll();
    }

    public boolean deleteById (Long id) {
        return accountDao.deleteById(id);
    }
}
