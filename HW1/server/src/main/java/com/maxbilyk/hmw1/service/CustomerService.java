package com.maxbilyk.hmw1.service;

import com.maxbilyk.hmw1.dao.AccountDao;
import com.maxbilyk.hmw1.dao.CustomerDao;
import com.maxbilyk.hmw1.domain.Account;
import com.maxbilyk.hmw1.domain.Customer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CustomerService {

    private final CustomerDao customerDao;
    private final AccountDao accountDao;

    private long customerId = 1;
    private long accountId = 1;

    public CustomerService(CustomerDao customerDao, AccountDao accountDao) {
        this.customerDao = customerDao;
        this.accountDao = accountDao;
    }

    public List<Customer> findAll() {
        return customerDao.findAll();
    }

    public Customer getOne(Long id) {
        return customerDao.getOne(id);
    }

    public Customer save(Customer customer) {
        customer.setId(customerId++);
        return customerDao.save(customer);
    }

    public void saveAll(List<Customer> customers) {
        for (Customer customer: customers) {
            customer.setId(customerId++);
        }
        customerDao.saveAll(customers);
    }

    public void deleteAll(List<Customer> customers) {
        customerDao.deleteAll(customers);
    }

    public boolean delete(Customer customer) {
        return customerDao.delete(customer);
    }

    public boolean deleteById(Long id) {
        return customerDao.deleteById(id);
    }

    public Customer update(Customer customer) {
        return customerDao.update(customer);
    }

    public Customer createAccount(Long id, Account account) {
        account.setId(accountId++);
        account.setNumber(UUID.randomUUID().toString());
        Customer customer = customerDao.getOne(id).addAccount(account);
        accountDao.save(account);
        return customer;
    }

    public Customer deleteAccount(Long customerId, Long accountId) {
        customerDao.deleteById(accountId);
        return customerDao.getOne(customerId).deleteAccount(accountId);
    }
}
