package com.maxbilyk.hmw1.controller;

import com.maxbilyk.hmw1.domain.Account;
import com.maxbilyk.hmw1.service.AccountService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/* Без анотации возникает ошибка: Could not autowire. No beans of 'AccountService' type found.  */
@EnableAutoConfiguration

@CrossOrigin
@RestController
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("account")
    public List<Account> getAccounts () {
        return accountService.findAll();
    }

    @PatchMapping("account/up")
    public Account toUpAccount (@RequestParam(value = "number") String number,
                                @RequestParam(value = "amount") Double amount)
    {
        return accountService.toUpAccount(number, amount);
    }

    @PutMapping("account/transfer")
    public Account transferMoney (@RequestParam(value = "fromNumber") String fromNumber,
                                  @RequestParam(value = "toNumber") String toNumber,
                                  @RequestParam(value = "amount") Double amount){
        return accountService.transferMoney(fromNumber, toNumber, amount);
    }

    @PatchMapping("account/withdraw")
    public Account withdrawMoney (@RequestParam(value = "number") String number,
                                  @RequestParam(value = "amount") Double amount)
    {
        return accountService.withdrawMoney(number, amount);
    }

    @DeleteMapping("account/{id}")
    public boolean deleteById(@PathVariable Long id) {
        return accountService.deleteById(id);
    }
}
