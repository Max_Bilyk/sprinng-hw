package com.maxbilyk.hmw1.enumable;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP;
}
