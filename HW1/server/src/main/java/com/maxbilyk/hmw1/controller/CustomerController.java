package com.maxbilyk.hmw1.controller;

import com.maxbilyk.hmw1.domain.Account;
import com.maxbilyk.hmw1.domain.Customer;
import com.maxbilyk.hmw1.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/customersAll")
    public void saveAll (@RequestBody List<Customer> customers) {
        customerService.saveAll(customers);
    }

    @GetMapping("/customers")
    public List<Customer> getAll() {
        return customerService.findAll();
    }

    @GetMapping("/customers/{id}")
    public Customer getById(@PathVariable Long id) {
        return customerService.getOne(id);
    }

    @PostMapping("/customers")
    public Customer create(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @DeleteMapping("/customers/{id}")
    public boolean deleteById(@PathVariable Long id) {
        return customerService.deleteById(id);
    }

    @PutMapping("/customers")
    public Customer update(@RequestBody Customer customer) {
        return customerService.update(customer);
    }

    @PostMapping("/customers/{id}/account")
    public Customer createAccount (@PathVariable Long id, @RequestBody Account account) {
        return customerService.createAccount(id, account);
    }

    @DeleteMapping("/customers/{id}/account/{accountId}")
    public void deleteAccount(@PathVariable Long id, @PathVariable Long accountId){
        customerService.deleteAccount(id, accountId);
    }
}
