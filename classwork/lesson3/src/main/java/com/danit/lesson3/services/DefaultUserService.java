package com.danit.lesson3.services;

//import com.danit.lesson3.dao.UserDao;
//import com.danit.lesson3.domains.User;

import java.util.List;

public class DefaultUserService implements UserService{
    private UserDao userDao;

    public DefaultUserService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public User getById(Long userId) {
        return userDao.getById(userId);
    }
}
