package com.danit.lesson3.dao;

import com.danit.lesson3.domains.User;

import java.util.List;

public interface UserDao {
    List<User> getAll();
    User getById(Long id);
}
