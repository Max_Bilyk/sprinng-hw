package com.danit.lesson3;

import org.h2.jdbcx.JdbcDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@SpringBootApplication
@PropertySource("classpath:application.properties")
public class Lesson3Application {

    public static void main(String[] args) {
        SpringApplication.run(Lesson3Application.class, args);
    }

//    @Value("${jdbc.driverClassName}")
//    private String driverClassName;
//
//    @Value("${jdbc.url}")
//    private String jdbcUrl;
//
//    @Value("${jdbc.username}")
//    private String userName;
//
//    @Value("${jdbc.password}")
//    private String password;
//
//    @Bean
//    public DataSource dataSource() {
//        JdbcDataSource dataSource = new JdbcDataSource();
//        dataSource.setUrl(jdbcUrl);
//        dataSource.setUser(userName);
//        dataSource.setPassword(password);
//        return dataSource;
//    }
}
