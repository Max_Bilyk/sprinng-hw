package com.danit.lesson3.domains;

import lombok.Data;

@Data
public class User {
    private final String firstName;
    private final String lastName;
    private final String email;
}
