package com.danit.lesson3.services;

import com.danit.lesson3.domains.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
    User getById(Long id);
}
