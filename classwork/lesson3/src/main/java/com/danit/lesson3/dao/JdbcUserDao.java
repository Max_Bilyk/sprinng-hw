package com.danit.lesson3.dao;

import com.danit.lesson3.domains.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcUserDao implements UserDao {
    private JdbcTemplate jdbcTemplate;

    public JdbcUserDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<User> getAll() {

        return null;
    }

    @Override
    public User getById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM TBL_EMPLOYEES WHERE id = ?",
                new Object[]{id},
                new BeanPropertyRowMapper<>(User.class));
    }
}
