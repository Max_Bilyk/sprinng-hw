package com.danit.task;

public class Address {
    private String street;
    private String building;
    private int apt;

    public Address() {}

    public Address(String street, String building, int apt) {
        this.street = street;
        this.building = building;
        this.apt = apt;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public int getApt() {
        return apt;
    }

    public void setApt(int apt) {
        this.apt = apt;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", apt=" + apt +
                '}';
    }
}
