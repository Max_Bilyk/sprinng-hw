DROP TABLE IF EXISTS TBL_ACCOUNTS;
DROP TABLE IF EXISTS TBL_CUSTOMERS;

CREATE TABLE TBL_ACCOUNTS
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    num   VARCHAR(250) NOT NULL,
    currency VARCHAR (250) NOT NULL,
    balance DOUBLE     NOT NULL
);

CREATE TABLE TBL_CUSTOMERS
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    name  VARCHAR(250) NOT NULL,
    email VARCHAR(250) NOT NULL,
    age   INT          NOT NULL
);