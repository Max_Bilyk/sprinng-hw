package com.homework.hw.models;

import com.homework.hw.domain.Currency;

import java.util.Objects;

public class Account {
    private long id;
    private String number;
    private String currency;
    private Double balance;
    private Customer accOwner;

    public Account() {}

    public Account(long id, String number) {
        this.id = id;
        this.number = number;
    }

    public Account(String currency, Customer accOwner) {
        this.currency = currency;
        this.accOwner = accOwner;
    }

    public Account(long id, String number, String currency, Double balance) {
        this.id = id;
        this.number = number;
        this.currency = currency;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Customer getAccOwner() {
        return accOwner;
    }

    public void setAccOwner(Customer accOwner) {
        this.accOwner = accOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id && number.equals(account.number) && currency == account.currency && Objects.equals(balance, account.balance) && accOwner.equals(account.accOwner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, currency, balance, accOwner);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", currency=" + currency +
                ", balance=" + balance +
                ", accOwner=" + accOwner +
                '}';
    }
}
