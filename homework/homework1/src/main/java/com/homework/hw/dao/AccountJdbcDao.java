package com.homework.hw.dao;

import com.homework.hw.models.Account;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountJdbcDao implements Dao<Account> {
    private DataSource dataSource;

    public AccountJdbcDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public List<Account> findAll() {
        Connection connection = null;
        List<Account> accounts = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM TBL_ACCOUNTS");
            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String number = resultSet.getString(2);
                String currency = resultSet.getString(3);
                Double balance = resultSet.getDouble(4);
                accounts.add(new Account(id, number, currency, balance));
            }
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            if (connection != null){
                try {
                    connection.close();
                }catch (SQLException exception){
                    exception.printStackTrace();
                }
            }
        }
        return accounts;
    }

    @Override
    public Account getOne(Long id) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TBL_ACCOUNTS WHERE id=?");
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                long accountId = resultSet.getLong(1);
                String number = resultSet.getString(2);
                String currency = resultSet.getString(3);
                Double balance = resultSet.getDouble(4);

                return new Account(accountId, number, currency, balance);
            }
        }catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                }catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public Account save(Account account) {
        return null;
    }

    @Override
    public void saveAll(List<Account> accounts) {

    }

    @Override
    public void deleteAll(List<Account> accounts) {
    }

    @Override
    public boolean delete(Account account) {
        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        return false;
    }
}
