package com.homework.hw.dao;

import com.homework.hw.models.Account;
import com.homework.hw.models.Customer;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerJdbcDao implements Dao<Customer> {
    private DataSource dataSource;

    public CustomerJdbcDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Customer> findAll() {
        Connection connection = null;
        List<Customer> customers = new ArrayList<>();
        List<Account> accounts = new ArrayList<>();

        try {
            connection = dataSource.getConnection();

            Statement statement = connection.createStatement();

            String query = "SELECT * FROM TBL_CUSTOMERS";

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString(2);
                String email = resultSet.getString(3);
                int age = resultSet.getInt(4);

                customers.add(new Customer(id, name, email, age, accounts));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return customers;
    }

    @Override
    public Customer getOne(Long id) {
        Connection connection = null;

        try {
            List<Account> customers = new ArrayList<>();
            connection = dataSource.getConnection();

            String query = "SELECT * FROM TBL_CUSTOMERS WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                long customerId = resultSet.getLong(1);
                String name = resultSet.getString(2);
                String email = resultSet.getString(3);
                int age = resultSet.getInt(4);

                return new Customer(customerId, name, email, age, customers);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public Customer save(Customer customer) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            String query = "INSERT INTO TBL_CUSTOMERS (name, email, age) VALUES (?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getEmail());
            preparedStatement.setInt(3, customer.getAge());

            int executeResult = preparedStatement.executeUpdate();
            connection.commit();

            return executeResult > 0 ?
                    new Customer(
                            customer.getName(),
                            customer.getEmail(),
                            customer.getAge()
                    ) : null;

        } catch (SQLException exception) {
            exception.printStackTrace();

            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public void saveAll(List<Customer> customers) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();

            for (Customer customer : customers) {
                save(customer);
            }

        } catch (SQLException exception) {
            exception.printStackTrace();

            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();

            for (Customer customer : customers) {
                deleteById(customer.getId());
            }

        } catch (SQLException exception) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean delete(Customer customer) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();

            if (findAll().contains(customer)) {
                deleteById(customer.getId());
                return true;
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();

            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        }

        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            String query = "DELETE FROM TBL_CUSTOMERS WHERE id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);

            int executeResult = preparedStatement.executeUpdate();
            connection.commit();

            return executeResult > 0;

        } catch (SQLException exception) {
            exception.printStackTrace();

            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return false;
    }

    public boolean update(Customer customer) {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            String query = "UPDATE TBL_CUSTOMERS SET name = ?, email = ?, age = ? WHERE id = ?";

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getEmail());
            preparedStatement.setInt(3, customer.getAge());
            preparedStatement.setLong(4, customer.getId());

            int executeResult = preparedStatement.executeUpdate();
            connection.commit();

            return executeResult > 0;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        }

        return false;
    }
}
