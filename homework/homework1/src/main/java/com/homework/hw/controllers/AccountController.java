package com.homework.hw.controllers;

import com.homework.hw.models.Account;
import com.homework.hw.services.I_Service;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AccountController {
    private I_Service<Account> accountIService;

    public AccountController(I_Service<Account> accountIService) {
        this.accountIService = accountIService;
    }
}
