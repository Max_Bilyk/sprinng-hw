package com.homework.hw.controllers;

import com.homework.hw.models.Customer;
import com.homework.hw.services.CustomerService;
import com.homework.hw.services.I_Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    // TODO Создать счет для конкретного пользователя
    // TODO Удалить счет у пользователя

    private I_Service<Customer> customerIService;
    private CustomerService customerService;

    public CustomerController(I_Service<Customer> customerIService, CustomerService customerService){
        this.customerIService = customerIService;
        this.customerService = customerService;
    }

    @GetMapping("/customers")
    public List<Customer> getAll () {
        return customerIService.findAll();
    }

    @GetMapping("/customers/{id}")
    public Customer getById (@PathVariable Long id) {
        return customerIService.getOne(id);
    }

    @PostMapping("/customers")
    public Customer create (@RequestBody Customer customer) {
        return customerIService.save(customer);
    }

    @DeleteMapping("/customers/{id}")
    public boolean deleteById (@PathVariable Long id) {
        return customerIService.deleteById(id);
    }

    @DeleteMapping("/customers")
    public boolean delete(@RequestBody  Customer customer) {
        return customerIService.delete(customer);
    }

    @PutMapping("/customers")
    public boolean update(@RequestBody Customer customer) {
        return customerService.update(customer);
    }
}
