package com.homework.hw.services;

import com.homework.hw.dao.Dao;
import com.homework.hw.models.Account;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService implements I_Service<Account> {
    private Dao<Account> accountDao;

    public AccountService(Dao<Account> accountJdbcDao){
        this.accountDao = accountJdbcDao;
    }


    @Override
    public List<Account> findAll() {
        return accountDao.findAll();
    }

    @Override
    public Account getOne(Long id) {
        return accountDao.getOne(id);
    }

    @Override
    public Account save(Account account) {
        return null;
    }

    @Override
    public void saveAll(List<Account> accounts) {

    }

    @Override
    public void deleteAll(List<Account> accounts) {
    }

    @Override
    public boolean delete(Account account) {
        return false;
    }

    @Override
    public boolean deleteById(Long id) {
        return false;
    }
}
