package com.homework.hw.services;

import com.homework.hw.dao.CustomerJdbcDao;
import com.homework.hw.models.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements I_Service<Customer>{

    private CustomerJdbcDao customerJdbcDao;

    public CustomerService(CustomerJdbcDao customerJdbcDao) {
        this.customerJdbcDao = customerJdbcDao;
    }

    @Override
    public List<Customer> findAll() {
        return customerJdbcDao.findAll();
    }

    @Override
    public Customer getOne(Long id) {
        return customerJdbcDao.getOne(id);
    }

    @Override
    public Customer save(Customer customer) {
        return customerJdbcDao.save(customer);
    }

    @Override
    public void saveAll(List<Customer> customers) {
        customerJdbcDao.saveAll(customers);
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        customerJdbcDao.deleteAll(customers);
    }

    @Override
    public boolean delete(Customer customer) {
        return customerJdbcDao.delete(customer);
    }

    @Override
    public boolean deleteById(Long id) {
        return customerJdbcDao.deleteById(id);
    }

    public boolean update (Customer customer) {
        return customerJdbcDao.update(customer);
    }
}
