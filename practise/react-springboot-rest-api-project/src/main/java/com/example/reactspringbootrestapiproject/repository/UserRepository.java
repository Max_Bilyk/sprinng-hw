package com.example.reactspringbootrestapiproject.repository;

import com.example.reactspringbootrestapiproject.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
